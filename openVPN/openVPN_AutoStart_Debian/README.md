# openVPN AutoStart Client-Konfiguration

Viele Menschen benutzen VPNs(Virtual Private Networks) um z.B. von außerhalb eines LANs auf die Inhalte dieses(LANs) zugreifen zu können. Dies wird oft verwendet um sicher, von Zuhause oder den WLAN-Hotspot im Café aus, Zugriff auf das Firmennetzwerk zu bekommen um dort Arbeiten zu können, oder einfach auch von draußen auf das Heimnetzwerk zugreifen zu können.

Unter Debian und deren Derivate kann die Konfiguration auf verschiedene weisen bewerkstelligt werden. Diese Anleitung ist ein Beispiel für die AutoStart-Konfiguration von openVPN unter systemd-basierte Distribuitionen mithilfe einer `.ovpn`-Konfigurationsdatei und gegebenfalls Kennwort zum anmelden.

Falls Sie sich aus bestimmten Gründen, auch im LAN wo sich der VPN-server befindet anmelden müssen kann es nötig sein die IP-Adresse des Servers (mit zugehörigen Port) in der `.ovpn`-Datei einzustellen.

## Getting started

Für die nächste Schritte benötigen Sie die OpenVPN ServerKonfigurationsdatei `.ovpn` und gegebenfalls dazugehörige Anmeldedaten (**OpenVPN/IKEv2 user** und **OpenVPN/IKEv2 password**).

Im Fall der Verwendung eines Kommerzielen-Dienstes, melden Sie sich auf die Webseite des Dienstleisters an um die Daten zu erlangen und die **.ovpn**-Datei herunterzuladen.

Falls Sie openVPN noch nicht installiert haben, vergewissern Sie sich dass Ihr Betriebssystem auf dem aktuellsten Stand ist (`sudo apt update`), mit dem Befehl:

```
sudo apt install openvpn
```
installieren Sie das Paket.


## openVPN AutoStart Konfiguration

Öffnen Sie die openvpn-Konfigurationsdatei `/etc/default/openvpn` mit einen beliebigen Texteditor:

```
sudo nano /etc/default/openvpn
```

und entkommentieren Sie (entfernen Sie das `#` vor):

```
AUTOSTART="all"
```
, dann speichern (`Strg+S`) Sie die Datei und schließen (`Strg+X`) Sie sie.


Kopieren Sie anschließend die `.ovpn`-Datei in das Verzeichnis `/etc/openvpn` und benennen Sie diese in `client.conf` um:

```
sudo cp /pfad/zu/client.ovpn /etc/openvpn/client.conf
```

### AnmeldeKennwort sicher speichern (Optional)

Da Sie, bei eine Anmeldung mit Kennwort, das Kennwort bei der Anmeldung eingeben müssen, ist es wichtig diese zu speichern, so dass openVPN es bei der Anmeldung abrufen kann um die Verbindung selbstständig herzustellen. Da das Kennwort nicht für andere Nutzer sichtbar sein sollte, ist eine Anpassung der Zugriffsrechte auf die Kennwortdatei empfehlenswert.

Zunächst öffnen Sie die zuletzt erstellte Konfigurationsdatei `client.conf` mit einen Texteditor:

```
sudo nano /etc/openvpn/client.conf
```

und bearbeiten dann die Zeile `auth-user-pass` in:

```
auth-user-pass pass
```
und speichern die Datei.

Diese Zeilenänderung führt dazu, dass das Kennwort bei der Anmeldung von der Datei `pass` abgerufen wird.


Danach erstellen Sie die Datei `pass`, öffnen sie mit einen beliebigen Texteditor:
```
sudo nano /etc/openvpn/pass
```

und geben das Kennwort für die VPN anmeldung ein:

```
password
```
, speichern (`Strg+S`) Sie anschließend die Datei und schließen (`Strg+X`) Sie sie.

Nachdem Sie das Kennwort gespeichert haben, ist es nun wichtig die Zugriffsrechte der Datei anzupassen so dass Sie nur vom Nutzer root gelesen werden kann:

```
sudo chmod 400 /etc/openvpn/pass
```

### LAN-IP Adresse des VPN-Servers Konfigurationsdatei eintragen

In manchen Fällen ist es Notwendig eine VPN-Verbindung herzustellen, obwohl man sich gerade(physisch) im lokalen Netzwerk des VPN-Servers befindet. Sollte dies der Fall sein, kann es hilfreich sein die lokale IP-Adresse des VPN-Servers und benutzten Port in die `remote`-Liste einzutragen.

Um dies zu konfigurieren, müssen Sie zunächst die lokale IP-Adresse sowie den Port des VPNs herrausfinden!

Dann öffnen Sie erneut die Datei `client.conf`:

```
sudo nano /etc/openvpn/client.conf
```

und fügen die IP-Adresse und Port unten in der `remote`-Liste:
(Beispiel: lokale IP: 192.168.1.33, Port: 1194)

```
client
dev tun
proto udp

remote 46.166.182.31 443
remote 46.166.182.31 1194
remote 192.168.1.33 1194

...
```

## Letzte Schritte

Nachdem Sie alle nötige Schritte erledigt haben können Sie nun den openVPN-Clientdienst aktivieren:

```
sudo systemctl enable openvpn@client.service
```

, den Daemon neuladen:

```
sudo systemctl daemon-reload
```

und zuletzt den Dienst starten:

```
sudo service openvpn@client start
```

Um zu prüfen, ob die Verbindung erfolgreich hergestellt wurde, können Sie zum Beispiel den Befehl `ip route` verwenden, bei Anmeldeverbindungen hilft `sudo service openvpn@client status` in der Regel weiter.

Viel Erfolg!

## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>

## License
The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).
